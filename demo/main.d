import std.stdio;
import std.file;

import gl3n.linalg;
import svgtiny;
import tess2;
import ttfparser;
import wayland.client;

import client;
import graphics.core;
import graphics.element.text;
import graphics.element.vector;
import graphics.renderer.vulkan;
import graphics.renderer.simple;
import input.core;
import input.pointer;

class CursorElement: VectorElement
{
	Seat seat;

	override void frame(uint delta)
	{
		Pointer pointer = seat.pointer;
		if (pointer)
			this.pos = pointer.pos - vec2(12.0, 12.0);

		super.frame(delta);
	}

	this(Seat seat)
	{
		this.seat = seat;

		string svgPath = "assets/circle.svg";
		ubyte[] svgData = cast(ubyte[]) read(svgPath);
		SVGTinyDiagram svg = new SVGTinyDiagram(svgData, "assets/circle.svg", 256, 256);

		super(vec2(0.0, 0.0), vec3(1.0, 1.0, 0.0), vec2(0.1, 0.1), svg.shape[0]);

	}
}

int main(string[] args) {
	VulkanRenderer renderer;
	Seat seat;

	void delegate() onConfigure = ()
	{
		/*
		 * Initiate first frame render. Frame callbacks will handle rendering further
		 * frames.
		 */
		renderer.frame(0);
	};

	Client client = new Client(onConfigure);
	renderer = new VulkanRenderer(
		client.wlSurface,
		client.wlDisplay,
		[640, 480]
	);

	client.init();
	client.onScale = &renderer.onScale;

	seat = new Seat(client.wlSeat);

	/* TODO: Get typeface from external path */
	string facePath = "assets/iosevka-regular.ttc";
	ubyte[] faceData = cast(ubyte[]) read(facePath);
	immutable TTFPFace face = new TTFPFace(faceData, 0);

	Tess tesselator = new Tess();

	float lastWidth = 0;
	for (ubyte i = 0; i < 5; ++i) {
		CharacterElement e = new CharacterElement(
			vec2(lastWidth, 60),
			vec3(1.0, 1.0, 1.0),
			tesselator,
			face,
			face.glyphIndex('A' + i)
		);
		e.scale = vec2(0.5, 0.5);
		lastWidth += e.boundingBox.size.x * e.scale.x + 5;

		renderer.addElement(e);
	}

	string svgPath = "assets/box.svg";
	ubyte[] svgData = cast(ubyte[]) read(svgPath);
	SVGTinyDiagram svg = new SVGTinyDiagram(svgData, "assets/box.svg", 256, 256);
	VectorElement e = new VectorElement(vec2(20, 200), vec3(1.0, 0.0, 0.0), shape: svg.shape[0]);
	e.rot = -10;
	renderer.addElement(e);

	svgPath = "assets/circle.svg";
	svgData = cast(ubyte[]) read(svgPath);
	svg = new SVGTinyDiagram(svgData, "assets/circle.svg", 256, 256);
	e = new VectorElement(vec2(150, 80), vec3(0.0, 1.0, 0.0), shape: svg.shape[0]);
	renderer.addElement(e);

	e = new CursorElement(seat);
	renderer.addElement(e);

	client.run();

	return 0;
}
