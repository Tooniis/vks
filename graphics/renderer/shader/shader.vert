#version 450

layout(push_constant) uniform constants {
	vec2 windowSize;
};

layout(binding = 0) uniform UBO {
	vec2 pos;
	float rot;
	vec2 scale;
};

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec2 fragPosition;
layout(location = 1) out vec3 fragColor;

void main()
{
	/* Compute rotation matrix */
	float rotrad = radians(rot);
	mat2 rotmat = mat2(
		cos(rotrad), -sin(rotrad),
		sin(rotrad),  cos(rotrad)
	);

	/* Apply transformation then map coordinates to 0..1 */
	fragPosition = (inPosition * scale * rotmat + pos) / windowSize * 2.0 - 1.0;

	fragColor = inColor;

	gl_Position = vec4(fragPosition, 0.0, 1.0);
}
