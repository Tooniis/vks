/*
 * Simple CPU-based rendering backend. Not functional yet (possibly never).
 */

module graphics.renderer.simple;

import std.stdio;
import std.format;
import std.exception;
import core.sys.posix.sys.mman;
import wayland.util.shm_helper;
import wayland.client;

import graphics.core;

final class simple_renderer: Renderer
{
	WlShm shm;
	WlBuffer buffer;
	ubyte[4]* data;

	uint[2] size;

	this(WlSurface wl_surface, WlShm wl_shm, uint[2] size)
	{
		super(wl_surface);

		this.shm = wl_shm;
		this.size = size;

		this.buffer = this.create_buffer(this.shm, this.size);
		surface.attach(buffer, 0, 0);

		box([0, 0], this.size, [0x00, 0x00, 0x00, 0xff]);

		surface.commit();
	}

	WlBuffer create_buffer(WlShm shm, uint[2] size)
	{
		/* Buffer size = width * height * 4 (each ARGB8888 pixel is 4 bytes long) */
		ulong buffer_size = this.size[0] * this.size[1] * 4;

		int pool_fd = createMmapableFile(buffer_size);
		data = cast(ubyte[4]*)mmap(
			null,
			buffer_size,
			PROT_READ | PROT_WRITE,
			MAP_SHARED,
			pool_fd,
			0
		);
		enforce(data !is MAP_FAILED);

		WlShmPool pool = enforce(shm.createPool(pool_fd, cast(int)buffer_size));
		buffer = pool.createBuffer(
			0, size[0], size[1], 4 * size[0], WlShm.Format.argb8888
		);

		return buffer;
	}

	override void frame(uint delta)
	{
		super.frame(delta);

		box([0, 0], [this.size[0], this.size[1]], [0x00, 0x00, 0x00, 0xff]);

		static uint counter = 0;
		counter += 1;
		if (counter > this.size[1])
			counter = 0;

		box([counter, counter], [20, 20], [0xff, 0x00, 0x00, 0xff]);

		surface.damageBuffer(0, 0, this.size[0], this.size[1]);
		surface.commit();
	}

	@trusted nothrow void box(uint[2] position, uint[2] size, ubyte[4] color)
	{
		if (position[0] > this.size[0] || position[1] > this.size[1])
			/* Box is out of bounds; nothing to draw. */
			return;

		foreach (uint i; [0, 1])
			if (position[i] + size[i] > this.size[i])
				/* Clip to bounds */
				size[i] = this.size[i] - position[i];

		for (uint y = position[1]; y < position[1] + size[1]; ++y) {
			for (uint x = position[0]; x < position[0] + size[0]; ++x) {
				data[y * this.size[0] + x] = color;
			}
		}
	}
}
