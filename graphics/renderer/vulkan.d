module graphics.renderer.vulkan;

import std.algorithm;
import std.file;
import std.exception;
import std.math;
import std.stdio;
import std.string;
import core.stdc.string;

import gl3n.linalg;
import vkd.vk;
import vkd.loader;
import wayland.client;

import graphics.core;

private {
	struct Instance {
		VkInstance instance;
		VkInstanceCmds cmds;
	}

	struct Device {
		VkDevice device;
		VkDeviceCmds cmds;

		QueueFamilyIndices indices;
	}

	class InstanceObject(T)
	{
		Instance instance;
		T object;
		void delegate(Instance instance, T object) destroy;

		this(
			Instance instance, T object,
			void delegate(Instance instance, T object) destroy
		) {
			this.instance = instance;
			this.object = object;
			this.destroy = destroy;
		}

		~this()
		{
			this.destroy(this.instance, this.object);
		}
	}

	class DeviceObject(T)
	{
		Device device;
		T object;
		void delegate(Device device, T object) destroy;

		this(Device device, T object, void delegate(Device device, T object) destroy)
		{
			this.device = device;
			this.object = object;
			this.destroy = destroy;
		}

		~this()
		{
			this.destroy(this.device, this.object);
		}
	}

	struct QueueFamilyIndices {
		uint graphicsFamily = -1;
		uint presentFamily = -1;

		@property bool complete()
		{
			return graphicsFamily != cast(uint) -1
				&& presentFamily != cast(uint) -1;
		}
	}

	struct SwapchainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities;
		VkSurfaceFormatKHR[] formats;
		VkPresentModeKHR[] presentModes;
	}

	struct Image {
		VkImage image;
		VkDeviceMemory memory;
		VkImageView view;
	};

	struct Pipeline {
		VkPipeline pipeline;
		VkPipelineLayout layout;
		VkShaderModule[] shaderModules;
	};

	struct CommandPool {
		VkCommandPool pool;
		VkCommandBuffer[] buffers;
	}

	struct Buffer {
		VkBuffer buffer;
		VkDeviceMemory memory;
		ulong size;
	};

	struct Vertex {
		vec2 pos;
		vec3 color;
	}

	struct UBO {
		align (8):
		vec2 pos;
		float rot;
		vec2 scale;
	}

	static uint findMemoryType(
		Instance instance,
		VkPhysicalDevice physicalDevice,
		uint filter,
		VkMemoryPropertyFlags propertyFlags)
	{
		VkPhysicalDeviceMemoryProperties memoryProperties;

		instance.cmds.GetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

		for (uint index = 0; index < memoryProperties.memoryTypeCount; ++index) {
			if (
				filter & (1 << index) &&
				(memoryProperties.memoryTypes[index].propertyFlags & propertyFlags)
					== propertyFlags
			)
				return index;
		}

		throw new Exception("No suitable memory type found");
	}

	uint highestBit(uint mask)
	{
		uint offset;

		if (!mask)
			return 0;

		for (offset = 31; offset >= 0; --offset) {
			if (mask >> offset)
				return 1 << offset;
		}

		assert(0);
	}
}

final class VulkanRenderer: Renderer
{
	private {
		uint[2] baseSize;
		int scale = 1;
		bool sizeChanged = false;

		VkGlobalCmds global;

		/* Instance */
		Instance instance;
		const char*[] requiredInstanceExtensionNames = [
			VK_KHR_SURFACE_EXTENSION_NAME,
			VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME,
			VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
		];

		VkDebugReportCallbackEXT debugReportCallback;
		const char*[] validationLayerNames = [
			"VK_LAYER_KHRONOS_validation",
		];

		/* Device */
		VkPhysicalDevice physicalDevice = null;
		VkPhysicalDeviceProperties physicalDeviceProperties;
		QueueFamilyIndices queueFamilyIndices;
		Device device;
		const char*[] requiredDeviceExtensionNames = [
			VK_KHR_SWAPCHAIN_EXTENSION_NAME,
		];

		/* Queues */
		VkQueue graphicsQueue;
		VkQueue presentQueue;

		/* Surface */
		InstanceObject!VkSurfaceKHR vk_surface;

		/* Shaders */
		ubyte[] vert_shader_code;
		ubyte[] frag_shader_code;

		/* Swapchain */
		DeviceObject!VkSwapchainKHR swapchain;
		VkImage[] swapchainImages;
		DeviceObject!(VkImageView[]) swapchainImageViews;
		uint imageIndex = 0;
		DeviceObject!(VkFramebuffer[]) swapchainFramebuffers;

		SwapchainSupportDetails swapchainSupportDetails;
		VkSurfaceFormatKHR swapchainSurfaceFormat;

		/* MSAA */
		VkSampleCountFlagBits msaaSamples;
		DeviceObject!Image msaaImage;

		/* Render pass */
		DeviceObject!VkRenderPass renderPass;

		/* Pipeline */
		DeviceObject!Pipeline pipeline;

		/* Command buffer */
		DeviceObject!CommandPool commandPool;

		/* Semaphores */
		DeviceObject!VkSemaphore imageAvailableSemaphore;
		DeviceObject!VkSemaphore renderFinishedSemaphore;

		/* Fences */
		DeviceObject!VkFence inFlightFence;

		/* Buffers */
		DeviceObject!Buffer vertexBuffer;
		DeviceObject!Buffer indexBuffer;
		DeviceObject!Buffer uniformBuffer;

		/* Descriptor sets */
		DeviceObject!VkDescriptorPool descriptorPool;
		DeviceObject!VkDescriptorSetLayout descriptorSetLayout;
		VkDescriptorSet[] descriptorSets;

		Vertex[] vertices;
		uint[] indices;
		UBO[] ubos;
	}

	private DeviceObject!VkSemaphore createSemaphore(Device device)
	{
		VkSemaphore semaphore;
		VkResult result;

		VkSemaphoreCreateInfo semaphoreCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
		};

		result = device.cmds.CreateSemaphore(
			device.device,
			&semaphoreCreateInfo,
			null,
			&semaphore
		);
		if (result)
			throw new Exception("Failed to create semaphore");

		return new DeviceObject!VkSemaphore(
			device,
			semaphore,
			(Device device, VkSemaphore semaphore) {
				device.cmds.DestroySemaphore(device.device, semaphore, null);
			}
		);
	}

	private DeviceObject!VkFence createFence(Device device)
	{
		VkFence fence;
		VkResult result;

		VkFenceCreateInfo fenceCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
		};

		result = device.cmds.CreateFence(
			device.device,
			&fenceCreateInfo,
			null,
			&fence
		);
		if (result)
			throw new Exception("Failed to create fence");

		return new DeviceObject!VkFence (
			device,
			fence,
			(Device device, VkFence fence) {
				device.cmds.DestroyFence(device.device, fence, null);
			}
		);
	}

	private Instance createInstance(
		VkGlobalCmds global,
		const char*[] requiredInstanceExtensionNames,
		const char*[] validationLayerNames
	)
	{
		Instance instance;
		uint instanceExtensionCount = 0;
		VkExtensionProperties[] availableInstanceExtensions;
		string[] availableInstanceExtensionNames;
		bool extensionMissing = false;

		uint instanceLayerCount = 0;
		VkLayerProperties[] availableInstanceLayers;
		string[] availableInstanceLayerNames;
		bool layerMissing = false;

		VkResult result;

		VkApplicationInfo appInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_APPLICATION_INFO,
			pApplicationName: "Vks",
			applicationVersion: 1, /* TODO: use VK_MAKE_VERSION() */
			pEngineName: "No Engine",
			engineVersion: 1 /* TODO: use VK_MAKE_VERSION() */
		};

		/* Get available instance extensions */
		global.EnumerateInstanceExtensionProperties(null, &instanceExtensionCount, null);
		availableInstanceExtensions.length = instanceExtensionCount;
		global.EnumerateInstanceExtensionProperties(null,
				&instanceExtensionCount, availableInstanceExtensions.ptr);

		writeln("Extensions:");
		foreach (VkExtensionProperties extension; availableInstanceExtensions)
			writeln(fromStringz(extension.extensionName.ptr));

		/* Check if required extensions are available */
		foreach(VkExtensionProperties extension; availableInstanceExtensions) {
			availableInstanceExtensionNames ~=
				cast(string) fromStringz(extension.extensionName.ptr).dup;
		}
		foreach (const char* cName; requiredInstanceExtensionNames) {
			string name = cast(string) fromStringz(cName);
			if (!canFind(availableInstanceExtensionNames,  name)) {
				writeln("Required extension not available: ", name);
				extensionMissing = true;
			}
		}
		if (extensionMissing)
			throw new Exception("Missing instance extensions");

		/* Get available instance layers */
		global.EnumerateInstanceLayerProperties(&instanceLayerCount, null);
		availableInstanceLayers.length = instanceLayerCount;
		global.EnumerateInstanceLayerProperties(
			&instanceLayerCount,
			availableInstanceLayers.ptr
		);

		writeln("Instance validation layers:");
		foreach (VkLayerProperties layer; availableInstanceLayers)
			writeln(layer.layerName);

		/* Check if validation layers are available */
		foreach(VkLayerProperties layer; availableInstanceLayers) {
			availableInstanceLayerNames ~=
				cast(string) fromStringz(layer.layerName.ptr).dup;
		}
		foreach (const char* cName; validationLayerNames) {
			string name = cast(string) fromStringz(cName);
			if (!canFind(availableInstanceLayerNames,  name)) {
				writeln("Required validation layer not available: ", name);
				layerMissing = true;
			}
		}
		if (layerMissing)
			throw new Exception("Missing instance layers");

		VkInstanceCreateInfo instanceCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
			pApplicationInfo: &appInfo,
			enabledExtensionCount: cast(uint) requiredInstanceExtensionNames.length,
			ppEnabledExtensionNames: requiredInstanceExtensionNames.ptr,
			enabledLayerCount: cast(uint) validationLayerNames.length,
			ppEnabledLayerNames: validationLayerNames.ptr,
		};

		/* Create instance */
		result = global.CreateInstance(&instanceCreateInfo, null, &instance.instance);
		if (result)
			throw new Exception("Failed to create Vulkan instance");

		instance.cmds = new VkInstanceCmds(instance.instance, global);

		return instance;
	}

	private VkDebugReportCallbackEXT createDebugReportCallback(
		Instance instance,
		PFN_vkDebugReportCallbackEXT
		callbackfn
	) {
		VkDebugReportCallbackEXT callback;
		VkResult result;

		VkDebugReportCallbackCreateInfoEXT debugReportCallbackCreateInfo = {
			sType: VkStructureType.
				VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
			flags: VkDebugReportFlagBitsEXT.VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
				VkDebugReportFlagBitsEXT.VK_DEBUG_REPORT_WARNING_BIT_EXT |
				VkDebugReportFlagBitsEXT.
					VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
				VkDebugReportFlagBitsEXT.VK_DEBUG_REPORT_ERROR_BIT_EXT |
				VkDebugReportFlagBitsEXT.VK_DEBUG_REPORT_DEBUG_BIT_EXT,
			pfnCallback: callbackfn,
			pUserData: cast(void*) this
		};

		result = instance.cmds.CreateDebugReportCallbackEXT(
			instance.instance,
			&debugReportCallbackCreateInfo,
			null,
			&callback
		);
		if (result)
			throw new Exception("Failed to create debug report callback");

		return callback;
	}

	private InstanceObject!VkSurfaceKHR createSurface(
		Instance instance,
		WlDisplay display,
		WlSurface wlSurface
	)
	{
		VkSurfaceKHR vkSurface;
		VkResult result;

		VkWaylandSurfaceCreateInfoKHR surfaceCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR,
			display: display.native,
			surface: wlSurface.proxy
		};

		/* Create surface */
		result = instance.cmds.CreateWaylandSurfaceKHR(
			instance.instance,
			&surfaceCreateInfo,
			null,
			&vkSurface
		);
		if (result)
			throw new Exception("Failed to create Vulkan surface");

		return new InstanceObject!VkSurfaceKHR(
			instance,
			vkSurface,
			(Instance instance, VkSurfaceKHR surface) {
				instance.cmds.DestroySurfaceKHR(instance.instance, surface, null);
			}
		);
	}

	private QueueFamilyIndices findQueueFamilyIndices(
		Instance instance,
		VkPhysicalDevice physicalDevice,
		WlDisplay display
	) {
		QueueFamilyIndices indices;
		uint queueFamilyCount;
		VkQueueFamilyProperties[] queueFamilies;

		/* Get queue families available on physical device */
		instance.cmds.GetPhysicalDeviceQueueFamilyProperties(
			physicalDevice,
			&queueFamilyCount,
			null);
		queueFamilies.length = queueFamilyCount;
		instance.cmds.GetPhysicalDeviceQueueFamilyProperties(
			physicalDevice,
			&queueFamilyCount,
			queueFamilies.ptr
		);

		/* Get indices of queue families */
		for(uint i = 0; i < queueFamilyCount; i++) {
			VkQueueFamilyProperties queueFamily = queueFamilies[i];

			if (queueFamily.queueFlags & VkQueueFlagBits.VK_QUEUE_GRAPHICS_BIT)
				indices.graphicsFamily = i;

			uint presentSupport =
				instance.cmds.GetPhysicalDeviceWaylandPresentationSupportKHR(
				physicalDevice,
				i,
				display.native
			);
			if (presentSupport)
				indices.presentFamily = i;

			/* Break if all family indices are found */
			if (indices.complete)
				break;
		}

		return indices;
	}

	private SwapchainSupportDetails getSwapchainSupportDetails(
		Instance instance,
		VkPhysicalDevice physicalDevice,
		VkSurfaceKHR surface
	) {
		SwapchainSupportDetails details;
		uint formatCount;
		uint presentModeCount;

		/* Get physical device surface capabilities */
		instance.cmds.GetPhysicalDeviceSurfaceCapabilitiesKHR(
			physicalDevice,
			surface,
			&details.capabilities);

		/* Get available surface formats on physical device */
		instance.cmds.GetPhysicalDeviceSurfaceFormatsKHR(
			physicalDevice,
			surface,
			&formatCount,
			null
		);
		details.formats.length = formatCount;
		instance.cmds.GetPhysicalDeviceSurfaceFormatsKHR(
			physicalDevice,
			surface,
			&formatCount,
			details.formats.ptr
		);

		/* Get available surface present modes on physical device */
		instance.cmds.GetPhysicalDeviceSurfacePresentModesKHR(
			physicalDevice,
			surface,
			&presentModeCount,
			null
		);
		details.presentModes.length = presentModeCount;
		instance.cmds.GetPhysicalDeviceSurfacePresentModesKHR(
			physicalDevice,
			surface,
			&presentModeCount,
			details.presentModes.ptr
		);

		return details;
	}

	private VkPhysicalDevice getPhysicalDevice(
		Instance instance,
		WlDisplay display,
		const char*[] requiredDeviceExtensionNames,
		VkSurfaceKHR surface,
		SwapchainSupportDetails *swapchainSupportDetails 
	) {
		VkPhysicalDevice physicalDevice = null;
		VkPhysicalDevice alternativePhysicalDevice = null;
		uint deviceCount = 0;
		VkPhysicalDevice[] physicalDevices;
		VkPhysicalDeviceProperties deviceProperties;
		QueueFamilyIndices indices;
		uint extensionCount = 0;
		VkExtensionProperties[] availableDeviceExtensions;
		string[] availableDeviceExtensionNames;
		bool extensionsAvailable = true;

		/* Enumerate physical devices */
		instance.cmds.EnumeratePhysicalDevices(instance.instance, &deviceCount, null);
		if (deviceCount == 0)
			throw new Exception("Failed to find GPUs with Vulkan support");

		physicalDevices.length = deviceCount;
		VkResult result = instance.cmds.EnumeratePhysicalDevices(
			instance.instance,
			&deviceCount,
			physicalDevices.ptr
		);
		if(result)
			throw new Exception("Failed to enumerate physical devices");

		/* Get first suitable physical device */
		foreach(VkPhysicalDevice device; physicalDevices) {
			/* Check if required queue families are available */
			indices = findQueueFamilyIndices(instance, device, display);
			if(!indices.complete)
				continue;

			/* Check if required extensions are available */
			instance.cmds.EnumerateDeviceExtensionProperties(
				device,
				null,
				&extensionCount,
				null
			);
			availableDeviceExtensions.length = extensionCount;
			instance.cmds.EnumerateDeviceExtensionProperties(
				device,
				null,
				&extensionCount,
				availableDeviceExtensions.ptr
			);

			foreach(VkExtensionProperties extension; availableDeviceExtensions) {
				availableDeviceExtensionNames ~=
					cast(string) fromStringz(extension.extensionName.ptr).dup;
			}
			foreach (const char* cName; requiredDeviceExtensionNames) {
				string name = cast(string) fromStringz(cName);
				if (!canFind(availableDeviceExtensionNames, name)) {
					extensionsAvailable = false;
					break;
				}
			}
			if(!extensionsAvailable)
				continue;

			/* Check swapchain support details */
			*swapchainSupportDetails =
				getSwapchainSupportDetails(instance, device, surface);
			/* Having at least 1 format and 1 present mode is enough */
			if(swapchainSupportDetails.formats.length == 0 ||
			   swapchainSupportDetails.presentModes.length == 0)
				continue;

			instance.cmds.GetPhysicalDeviceProperties(
				device,
				&deviceProperties
			);

			/* All requirements are met. Now select the best option. */

			/* Prefer an integrated GPU to save power */
			if (deviceProperties.deviceType ==
			    VkPhysicalDeviceType.VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) {
				physicalDevice = device;
				/* Best match found. Stop looking */
				break;
			}
			/* Pick discrete GPUs as an alternative option */
			else if (deviceProperties.deviceType ==
			    VkPhysicalDeviceType.VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
				alternativePhysicalDevice = device;
				/* Keep looking for a better option */
			}
			/* Pick virtual or unknown type physical devices as a last option */
			else if (!alternativePhysicalDevice) {
				alternativePhysicalDevice = device;
				/* Keep looking for a better option */
			}
		}

		if(physicalDevice)
			return physicalDevice;
		else if (alternativePhysicalDevice)
			return alternativePhysicalDevice;

		throw new Exception("Failed to find suitable GPU");

		return null;
	}

	private Device createDevice(
		Instance instance,
		VkPhysicalDevice physicalDevice,
		QueueFamilyIndices indices,
		const char*[] requiredDeviceExtensionNames,
		const char*[] validationLayerNames
	) {
		Device device;
		VkDeviceQueueCreateInfo[] deviceQueueCreateInfos;
		uint[] uniqueQueueFamilies;
		float queuePriority = 1.0;
		uint deviceLayerCount;
		VkLayerProperties[] availableDeviceLayers;
		string[] availableDeviceLayerNames;
		bool layerMissing = false;
		VkResult result;

		if (indices.graphicsFamily == indices.presentFamily)
			uniqueQueueFamilies = [indices.graphicsFamily];
		else {
			uniqueQueueFamilies = [indices.graphicsFamily, indices.presentFamily];
			sort(uniqueQueueFamilies);
		}

		foreach (uint queueFamily; uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo deviceQueueCreateInfo = {
				sType: VkStructureType.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
				queueFamilyIndex: queueFamily,
				queueCount: 1,
				pQueuePriorities: &queuePriority
			};

			deviceQueueCreateInfos ~= deviceQueueCreateInfo;
		}

		/* Get available device validation layers */
		instance.cmds.EnumerateDeviceLayerProperties(
			physicalDevice,
			&deviceLayerCount,
			null
		);
		availableDeviceLayers.length = deviceLayerCount;
		instance.cmds.EnumerateDeviceLayerProperties(
			physicalDevice,
			&deviceLayerCount,
			availableDeviceLayers.ptr
		);

		writeln("Device layers:");
		foreach (VkLayerProperties layer; availableDeviceLayers)
			writeln(layer.layerName);

		/* Check if validation layers are available */
		foreach(VkLayerProperties layer; availableDeviceLayers) {
			availableDeviceLayerNames ~=
				cast(string) fromStringz(layer.layerName.ptr).dup;
		}
		foreach (const char* cName; validationLayerNames) {
			string name = cast(string) fromStringz(cName);
			if (!canFind(availableDeviceLayerNames, name)) {
				writeln("Required device validation layer not available: ", name);
				layerMissing = true;
			}
		}
		if (layerMissing)
			throw new Exception("Missing device layers");

		VkDeviceCreateInfo deviceCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
			queueCreateInfoCount: cast(uint) deviceQueueCreateInfos.length,
			pQueueCreateInfos: deviceQueueCreateInfos.ptr,
			pEnabledFeatures: null,
			enabledExtensionCount: cast(uint) requiredDeviceExtensionNames.length,
			ppEnabledExtensionNames: requiredDeviceExtensionNames.ptr,
			enabledLayerCount: cast(uint) validationLayerNames.length,
			ppEnabledLayerNames: validationLayerNames.ptr
		};

		result = instance.cmds.CreateDevice(
			physicalDevice,
			&deviceCreateInfo,
			null,
			&device.device
		);
		if (result)
			throw new Exception("Failed to create device");

		device.cmds = new VkDeviceCmds(device.device, instance.cmds);
		device.indices = indices;

		return device;
	}

	private DeviceObject!VkSwapchainKHR createSwapchain(
		Instance instance,
		Device device,
		VkPhysicalDevice physicalDevice,
		SwapchainSupportDetails details,
		VkSurfaceKHR surface,
		VkSurfaceFormatKHR surfaceFormat,
		uint[2] size
	) {
		VkExtent2D imageExtent;
		VkPresentModeKHR presentMode;
		uint minImageCount;
		VkSwapchainKHR swapchain;
		VkResult result;

		/* Configure swap extent */
		if (details.capabilities.currentExtent.width != uint.max)
			/* Resolution is not configurable. Pick current extent. */
			imageExtent = details.capabilities.currentExtent;
		else {
			/* Pick maximum possible resolution */
			imageExtent.width = max(
				details.capabilities.minImageExtent.width,
				min(details.capabilities.maxImageExtent.width, size[0])
			);
			imageExtent.height = max(
				details.capabilities.minImageExtent.height,
				min(details.capabilities.maxImageExtent.height, size[1])
			);
		}

		/* Pick present mode */
		/* Default to FIFO */
		presentMode = VkPresentModeKHR.VK_PRESENT_MODE_FIFO_KHR;
		/* Pick mailbox if available */
		foreach (VkPresentModeKHR mode; details.presentModes) {
			if (mode == VkPresentModeKHR.VK_PRESENT_MODE_MAILBOX_KHR)
				presentMode = mode;
		}

		/* Get minimum image count, then add one if possible */
		minImageCount = details.capabilities.minImageCount;
		if (
			minImageCount < details.capabilities.maxImageCount ||
			!details.capabilities.maxImageCount
		)
			minImageCount++;

		VkSwapchainCreateInfoKHR swapchainCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
			surface: surface,
			minImageCount: minImageCount,
			imageExtent: imageExtent,
			imageFormat: surfaceFormat.format,
			imageColorSpace: surfaceFormat.colorSpace,
			presentMode: presentMode,
			clipped: true,
			imageArrayLayers: 1,
			imageUsage: VkImageUsageFlagBits.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			preTransform: details.capabilities.currentTransform,
			compositeAlpha:
				VkCompositeAlphaFlagBitsKHR.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			oldSwapchain: this.swapchain ? this.swapchain.object : null,
		};
		if (device.indices.graphicsFamily == device.indices.presentFamily) {
			/* Same queue for graphics and presentation */
			swapchainCreateInfo.imageSharingMode =
				VkSharingMode.VK_SHARING_MODE_EXCLUSIVE;
		} else {
			/* Different queues for graphics and presentation */
			swapchainCreateInfo.imageSharingMode =
				VkSharingMode.VK_SHARING_MODE_CONCURRENT;
			swapchainCreateInfo.queueFamilyIndexCount = 2;
			swapchainCreateInfo.pQueueFamilyIndices = [
				device.indices.graphicsFamily,
				device.indices.presentFamily
			].ptr;
		}

		result = device.cmds.CreateSwapchainKHR(
			device.device,
			&swapchainCreateInfo,
			null,
			&swapchain
		);
		if (result)
			throw new Exception("Failed to create swapchain");

		return new DeviceObject!VkSwapchainKHR(
			device,
			swapchain,
			(Device device, VkSwapchainKHR swapchain) {
				device.cmds.DestroySwapchainKHR(
					device.device,
					swapchain,
					null
				);
			}
		);
	}

	private DeviceObject!(VkImageView[]) createImageViews(
		Device device,
		VkImage[] images,
		VkFormat imageFormat
	) {
		VkImageView[] imageViews;
		VkResult result;

		foreach (VkImage image; images) {
			VkImageView imageView;

			VkImageViewCreateInfo imageViewCreateInfo = {
				sType: VkStructureType.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				image: image,
				viewType: VkImageViewType.VK_IMAGE_VIEW_TYPE_2D,
				format: imageFormat,
				components: {
					r: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
					g: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
					b: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
					a: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
				},
				subresourceRange: {
					aspectMask: VkImageAspectFlagBits.VK_IMAGE_ASPECT_COLOR_BIT,
					baseMipLevel: 0,
					levelCount: 1,
					baseArrayLayer: 0,
					layerCount: 1
				}
			};

			result = device.cmds.CreateImageView(device.device,
				&imageViewCreateInfo,
				null,
				&imageView
			);
			if (result)
				throw new Exception("Failed to create image view");

			imageViews ~= imageView;
		}

		return new DeviceObject!(VkImageView[])(
			device,
			imageViews,
			(Device device, VkImageView[] imageViews) {
				foreach (VkImageView imageView; imageViews)
					device.cmds.DestroyImageView(
						device.device,
						imageView,
						null
					);
			}
		);
	}

	private DeviceObject!VkRenderPass createRenderPass(
		Device device,
		VkFormat imageFormat,
		VkSampleCountFlagBits samples
	) {
		VkRenderPass renderPass;
		VkResult result;

		VkAttachmentDescription[] attachmentDescriptions = [
			{
				format: imageFormat,
				samples: samples,
				loadOp: VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_CLEAR,
				storeOp: VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_STORE,
				stencilLoadOp: VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				stencilStoreOp:
					VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_DONT_CARE,
				initialLayout: VkImageLayout.VK_IMAGE_LAYOUT_UNDEFINED,
				finalLayout: VkImageLayout.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
			},
			{
				format: imageFormat,
				samples: VkSampleCountFlagBits.VK_SAMPLE_COUNT_1_BIT,
				loadOp: VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				storeOp: VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_STORE,
				stencilLoadOp: VkAttachmentLoadOp.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				stencilStoreOp:
					VkAttachmentStoreOp.VK_ATTACHMENT_STORE_OP_DONT_CARE,
				initialLayout: VkImageLayout.VK_IMAGE_LAYOUT_UNDEFINED,
				finalLayout: VkImageLayout.VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
			}
		];

		VkAttachmentReference[] colorAttachmentReferences = [
			{
				attachment: 0,
				layout: VkImageLayout.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
			},
		];

		VkAttachmentReference resolveAttachementReference = {
			attachment: 1,
			layout: VkImageLayout.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		};

		VkSubpassDescription[] subpassDescriptions = [
			{
				pipelineBindPoint:
					VkPipelineBindPoint.VK_PIPELINE_BIND_POINT_GRAPHICS,
				colorAttachmentCount: cast(uint) colorAttachmentReferences.length,
				pColorAttachments: colorAttachmentReferences.ptr,
				pResolveAttachments: &resolveAttachementReference
			}
		];

		VkSubpassDependency[] subpassDependencies = [
			{
				srcSubpass: VK_SUBPASS_EXTERNAL,
				dstSubpass: 0,
				srcStageMask: VkPipelineStageFlagBits.
					VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				dstStageMask: VkPipelineStageFlagBits.
					VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				srcAccessMask: 0,
				dstAccessMask: VkAccessFlagBits.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
			}
		];

		VkRenderPassCreateInfo renderPassCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
			attachmentCount: cast(uint) attachmentDescriptions.length,
			pAttachments: attachmentDescriptions.ptr,
			subpassCount: cast(uint) subpassDescriptions.length,
			pSubpasses: subpassDescriptions.ptr,
			dependencyCount: cast(uint) subpassDependencies.length,
			pDependencies: subpassDependencies.ptr
		};

		result = device.cmds.CreateRenderPass(
			device.device,
			&renderPassCreateInfo,
			null,
			&renderPass
		);
		if (result)
			throw new Exception("Failed to create render pass");

		return new DeviceObject!VkRenderPass(
			device,
			renderPass,
			(Device device, VkRenderPass renderPass) {
				device.cmds.DestroyRenderPass(
					device.device,
					renderPass,
					null
				);
			}
		);
	}

	private DeviceObject!Pipeline createPipeline(
		Device device,
		VkRenderPass renderPass,
		VkDescriptorSetLayout descriptorSetLayout,
		ubyte[] vertexShaderCode,
		ubyte[] fragmentShaderCode,
		VkSampleCountFlagBits samples,
		uint[2] size
	) {
		Pipeline pipeline;
		VkResult result;

		VkShaderModuleCreateInfo[] shaderModuleCreateInfos = [
			{
				sType: VkStructureType.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
				codeSize: vertexShaderCode.length,
				pCode: (cast(uint[]) vertexShaderCode).ptr
			},
			{
				sType: VkStructureType.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
				codeSize: fragmentShaderCode.length,
				pCode: (cast(uint[]) fragmentShaderCode).ptr
			}
		];

		foreach (VkShaderModuleCreateInfo shaderModuleCreateInfo; shaderModuleCreateInfos) {
			VkShaderModule shaderModule;

			result = device.cmds.CreateShaderModule(
				device.device,
				&shaderModuleCreateInfo,
				null,
				&shaderModule
			);

			if (result)
				throw new Exception("Failed to create shader module");

			pipeline.shaderModules ~= shaderModule;
		}

		VkPipelineShaderStageCreateInfo[] pipelineShaderStageCreateInfos = [
			{
				sType: VkStructureType.
					VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				stage: VkShaderStageFlagBits.VK_SHADER_STAGE_VERTEX_BIT,
				module_: pipeline.shaderModules[0],
				pName: toStringz("main")
			},
			{
				sType: VkStructureType.
					VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				stage: VkShaderStageFlagBits.VK_SHADER_STAGE_FRAGMENT_BIT,
				module_: pipeline.shaderModules[1],
				pName: toStringz("main")
			},
		];

		VkVertexInputBindingDescription[] bindingDescriptions = [
			{
				binding: 0,
				stride: Vertex.sizeof,
				inputRate: VkVertexInputRate.VK_VERTEX_INPUT_RATE_VERTEX
			}
		];

		VkVertexInputAttributeDescription[] attributeDescriptions = [
			{
				binding: 0,
				location: 0,
				format: VkFormat.VK_FORMAT_R32G32_SFLOAT,
				offset: Vertex.pos.offsetof
			},
			{
				binding: 0,
				location: 1,
				format: VkFormat.VK_FORMAT_R32G32B32_SFLOAT,
				offset: Vertex.color.offsetof
			}
		];

		VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInfo = {
			sType: VkStructureType.
				VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			vertexBindingDescriptionCount: cast(uint)bindingDescriptions.length,
			pVertexBindingDescriptions: bindingDescriptions.ptr,
			vertexAttributeDescriptionCount: cast(uint)attributeDescriptions.length,
			pVertexAttributeDescriptions: attributeDescriptions.ptr
		};

		VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo = {
			sType: VkStructureType.
				VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
			topology: VkPrimitiveTopology.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
			primitiveRestartEnable: false
		};

		VkViewport[] viewports = [
			{
				x: 0.0,
				y: 0.0,
				width: cast(float) size[0],
				height: cast(float) size[1],
				minDepth: 0.0,
				maxDepth: 1.0
			}
		];

		VkRect2D[] scissors = [	{{0, 0}, {size[0], size[1]}} ];

		VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo = {
			sType: VkStructureType.
				VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
			viewportCount: cast(uint) viewports.length,
			pViewports: viewports.ptr,
			scissorCount: cast(uint) scissors.length,
			pScissors: scissors.ptr
		};

		VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo = {
			sType: VkStructureType.
				VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
			depthClampEnable: false,
			rasterizerDiscardEnable: false,
			polygonMode: VkPolygonMode.VK_POLYGON_MODE_FILL,
			cullMode: VkCullModeFlagBits.VK_CULL_MODE_NONE, // TODO
			frontFace: VkFrontFace.VK_FRONT_FACE_CLOCKWISE,
			depthBiasEnable: false,
			depthBiasClamp: 0.0,
			lineWidth: 1.0
		};

		VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo = {
			sType: VkStructureType.
				VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
			rasterizationSamples: samples,
			sampleShadingEnable: false,
			alphaToOneEnable: false,
			alphaToCoverageEnable: false
		};

		VkPipelineColorBlendAttachmentState[] pipelineColorBlendAttachmentStates = [
			{
				blendEnable: false,
				colorBlendOp: VkBlendOp.VK_BLEND_OP_ADD,
				srcColorBlendFactor: VkBlendFactor.VK_BLEND_FACTOR_ONE,
				dstColorBlendFactor: VkBlendFactor.VK_BLEND_FACTOR_ONE,
				colorWriteMask:
					VkColorComponentFlagBits.VK_COLOR_COMPONENT_A_BIT |
					VkColorComponentFlagBits.VK_COLOR_COMPONENT_R_BIT |
					VkColorComponentFlagBits.VK_COLOR_COMPONENT_G_BIT |
					VkColorComponentFlagBits.VK_COLOR_COMPONENT_B_BIT
			}
		];

		VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo = {
			sType: VkStructureType.
				VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
			logicOpEnable: false,
			attachmentCount: cast(uint) pipelineColorBlendAttachmentStates.length,
			pAttachments: pipelineColorBlendAttachmentStates.ptr,
		};

		VkPushConstantRange[] pushConstantRanges = [
			{
				stageFlags: VkShaderStageFlagBits.VK_SHADER_STAGE_VERTEX_BIT,
				offset: 0,
				size: vec2.sizeof
			}
		];

		/* Create empty pipeline layout for now */
		VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
			setLayoutCount: 1,
			pSetLayouts: &descriptorSetLayout,
			pushConstantRangeCount: cast(uint) pushConstantRanges.length,
			pPushConstantRanges: pushConstantRanges.ptr
		};

		result = device.cmds.CreatePipelineLayout(
			device.device,
			&pipelineLayoutCreateInfo,
			null,
			&pipeline.layout
		);
		if (result)
			throw new Exception("Failed to create pipeline layout");

		VkGraphicsPipelineCreateInfo[] graphicsPipelineCreateInfos = [
			{
				sType: VkStructureType.
					VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
				stageCount: cast(uint) pipelineShaderStageCreateInfos.length,
				pStages: pipelineShaderStageCreateInfos.ptr,
				pVertexInputState: &vertexInputStateCreateInfo,
				pInputAssemblyState: &inputAssemblyStateCreateInfo,
				pTessellationState: null,
				pViewportState: &pipelineViewportStateCreateInfo,
				pRasterizationState: &pipelineRasterizationStateCreateInfo,
				pMultisampleState: &pipelineMultisampleStateCreateInfo,
				pDepthStencilState: null,
				pColorBlendState: &pipelineColorBlendStateCreateInfo,
				pDynamicState: null,
				layout: pipeline.layout,
				renderPass: renderPass,
				subpass: 0,
			}
		];

		result = device.cmds.CreateGraphicsPipelines(
			device.device,
			null,
			cast(uint) graphicsPipelineCreateInfos.length,
			graphicsPipelineCreateInfos.ptr,
			null,
			&pipeline.pipeline
		);
		if (result)
			throw new Exception("Failed to create pipeline");

		return new DeviceObject!Pipeline(
			device,
			pipeline,
			(Device device, Pipeline pipeline) {
				device.cmds.DestroyPipeline(
					device.device,
					pipeline.pipeline,
					null
				);

				device.cmds.DestroyPipelineLayout(
					device.device,
					pipeline.layout,
					null
				);

				foreach (VkShaderModule shaderModule; pipeline.shaderModules)
					device.cmds.DestroyShaderModule(
						device.device,
						shaderModule,
						null
					);
			}
		);
	}

	private DeviceObject!(VkFramebuffer[]) createFramebuffers(
		Device device,
		VkRenderPass renderPass,
		VkImageView msaaImageView,
		VkImageView[] imageViews,
		uint[2] size)
	{
		VkFramebuffer[] framebuffers;
		VkResult result;

		for (uint index = 0; index < imageViews.length; ++index) {
			VkFramebuffer framebuffer;
			VkImageView[] attachements = [msaaImageView, imageViews[index]];
			VkFramebufferCreateInfo framebufferCreateInfo = {
				sType: VkStructureType.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
				renderPass: renderPass,
				attachmentCount: cast(uint) attachements.length,
				pAttachments: attachements.ptr,
				width: size[0],
				height: size[1],
				layers: 1
			};

			result = device.cmds.CreateFramebuffer(
				device.device,
				&framebufferCreateInfo,
				null,
				&framebuffer
			);
			if (result)
				throw new Exception("Failed to create framebuffer");

			framebuffers ~= framebuffer;
		}

		return new DeviceObject!(VkFramebuffer[])(
			device,
			framebuffers,
			(Device device, VkFramebuffer[] framebuffers) {
				foreach (VkFramebuffer framebuffer; framebuffers)
					device.cmds.DestroyFramebuffer(
						device.device,
						framebuffer,
						null
					);
			}
		);
	}

	private DeviceObject!Buffer createBuffer(
		Device device,
		Instance instance,
		VkPhysicalDevice physicalDevice,
		ulong size,
		VkBufferUsageFlagBits usage
	) {
		Buffer buffer;
		VkMemoryRequirements memoryRequirements;
		VkResult result;

		/* Create buffer */
		VkBufferCreateInfo bufferCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			size: size,
			usage: usage,
			sharingMode: VkSharingMode.VK_SHARING_MODE_EXCLUSIVE
		};

		result = device.cmds.CreateBuffer(
			device.device,
			&bufferCreateInfo,
			null,
			&buffer.buffer
		);
		if (result)
			throw new Exception("Failed to create vertex buffer");

		device.cmds.GetBufferMemoryRequirements(
			device.device,
			buffer.buffer,
			&memoryRequirements
		);

		/* Allocate memory for buffer */
		VkMemoryAllocateInfo memoryAllocateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
			allocationSize: size,
			memoryTypeIndex: findMemoryType(
				instance,
				physicalDevice,
				memoryRequirements.memoryTypeBits,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
				VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
			),
		};

		result = device.cmds.AllocateMemory(
			device.device,
			&memoryAllocateInfo,
			null,
			&buffer.memory
		);
		if (result)
			throw new Exception("Failed to allocate memory for buffer");

		/* Bind allocated memory to buffer */
		device.cmds.BindBufferMemory(
			device.device,
			buffer.buffer,
			buffer.memory,
			0
		);

		buffer.size = size;

		return new DeviceObject!Buffer(
			device,
			buffer,
			(Device device, Buffer buffer) {
				device.cmds.DestroyBuffer(
					device.device,
					buffer.buffer,
					null
				);
				device.cmds.FreeMemory(device.device, buffer.memory, null);
			}
		);
	}

	private DeviceObject!VkDescriptorPool createDescriptorPool(Device device)
	{
		VkDescriptorPool descriptorPool;
		VkResult result;

		VkDescriptorPoolSize descriptorPoolSize = {
			type: VkDescriptorType.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
			descriptorCount: 1
		};

		VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
			maxSets: 1,
			poolSizeCount: 1,
			pPoolSizes: &descriptorPoolSize
		};

		result = device.cmds.CreateDescriptorPool(
			device.device,
			&descriptorPoolCreateInfo,
			null,
			&descriptorPool
		);
		if (result)
			throw new Exception("Failed to create descriptor pool");

		return new DeviceObject!VkDescriptorPool(
			device,
			descriptorPool,
			(Device device, VkDescriptorPool descriptorPool) {
				device.cmds.DestroyDescriptorPool(
					device.device,
					descriptorPool,
					null
				);
			}
		);
	}

	private DeviceObject!VkDescriptorSetLayout createDescriptorSetLayout(Device device)
	{
		VkDescriptorSetLayout descriptorSetLayout;
		VkResult result;

		VkDescriptorSetLayoutBinding[] descriptorSetLayoutBindings = [
			{
				binding: 0,
				descriptorType:
					VkDescriptorType.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
				descriptorCount: 1,
				stageFlags: VkShaderStageFlagBits.VK_SHADER_STAGE_VERTEX_BIT,
			}
		];

		VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
			bindingCount: cast(uint) descriptorSetLayoutBindings.length,
			pBindings: descriptorSetLayoutBindings.ptr
		};

		result = device.cmds.CreateDescriptorSetLayout(
			device.device,
			&descriptorSetLayoutCreateInfo,
			null,
			&descriptorSetLayout
		);
		if (result)
			throw new Exception("Failed to create descriptor set layout");

		return new DeviceObject!VkDescriptorSetLayout(
			device,
			descriptorSetLayout,
			(Device device, VkDescriptorSetLayout descriptorSetLayout) {
				device.cmds.DestroyDescriptorSetLayout(
					device.device,
					descriptorSetLayout,
					null
				);
			}
		);
	}

	private VkDescriptorSet[] allocateDescriptorSets(
		Device device,
		VkDescriptorPool descriptorPool,
		VkDescriptorSetLayout[] descriptorSetLayouts
	) {
		VkDescriptorSet[] descriptorSets;
		VkResult result;

		descriptorSets.length = descriptorSetLayouts.length;

		VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
			descriptorPool: descriptorPool,
			descriptorSetCount: cast(uint) descriptorSetLayouts.length,
			pSetLayouts: descriptorSetLayouts.ptr
		};

		result = device.cmds.AllocateDescriptorSets(
			device.device,
			&descriptorSetAllocateInfo,
			descriptorSets.ptr
		);
		if (result)
			throw new Exception("Failed to allocate descriptor sets");

		return descriptorSets;
	}

	private DeviceObject!Image createImage(
		Device device,
		Instance instance,
		VkPhysicalDevice physicalDevice,
		uint[2] size,
		VkFormat format,
		VkImageType type,
		VkSampleCountFlagBits samples,
		VkImageTiling tiling,
		VkImageUsageFlagBits usage,
		VkMemoryPropertyFlags memoryProperties
	) {
		Image image;
		VkMemoryRequirements memoryRequirements;
		VkResult result;

		VkImageCreateInfo imageCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
			imageType: type,
			format: format,
			extent: {size[0], size[1], 1},
			mipLevels: 1,
			arrayLayers: 1,
			samples: samples,
			tiling: tiling,
			usage: usage,
			initialLayout: VkImageLayout.VK_IMAGE_LAYOUT_UNDEFINED
		};

		if (device.indices.graphicsFamily == device.indices.presentFamily) {
			/* Same queue for graphics and presentation */
			imageCreateInfo.sharingMode =
				VkSharingMode.VK_SHARING_MODE_EXCLUSIVE;
		} else {
			/* Different queues for graphics and presentation */
			imageCreateInfo.sharingMode =
				VkSharingMode.VK_SHARING_MODE_CONCURRENT;
			imageCreateInfo.queueFamilyIndexCount = 2;
			imageCreateInfo.pQueueFamilyIndices = [
				device.indices.graphicsFamily,
				device.indices.presentFamily
			].ptr;
		}

		result = device.cmds.CreateImage(
			device.device,
			&imageCreateInfo,
			null, &image.image
		);
		if (result)
			throw new Exception("Failed to create image");

		device.cmds.GetImageMemoryRequirements(
			device.device,
			image.image,
			&memoryRequirements
		);

		VkMemoryAllocateInfo memoryAllocateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
			allocationSize: memoryRequirements.size,
			memoryTypeIndex: findMemoryType(
				instance,
				physicalDevice,
				memoryRequirements.memoryTypeBits,
				memoryProperties
			),
		};

		result = device.cmds.AllocateMemory(
			device.device,
			&memoryAllocateInfo,
			null,
			&image.memory
		);
		if (result)
			throw new Exception("Failed to allocate image memory");

		device.cmds.BindImageMemory(device.device, image.image, image.memory, 0);

		VkImageViewCreateInfo imageViewCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			image: image.image,
			viewType: VkImageViewType.VK_IMAGE_VIEW_TYPE_2D,
			format: format,
			components: {
				r: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
				g: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
				b: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
				a: VkComponentSwizzle.VK_COMPONENT_SWIZZLE_IDENTITY,
			},
			subresourceRange: {
				aspectMask: VkImageAspectFlagBits.VK_IMAGE_ASPECT_COLOR_BIT,
				baseMipLevel: 0,
				levelCount: 1,
				baseArrayLayer: 0,
				layerCount: 1
			}
		};

		result = device.cmds.CreateImageView(device.device,
			&imageViewCreateInfo,
			null,
			&image.view
		);
		if (result)
			throw new Exception("Failed to create image view");

		return new DeviceObject!Image(
			device,
			image,
			(Device device, Image image) {
				device.cmds.DestroyImageView(device.device, image.view, null);
				device.cmds.DestroyImage(device.device, image.image, null);
				device.cmds.FreeMemory(device.device, image.memory, null);
			}
		);
	}

	private DeviceObject!CommandPool createCommandPool(Device device)
	{
		CommandPool commandPool;
		VkResult result;

		VkCommandPoolCreateInfo commandPoolCreateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
			flags: VkCommandPoolCreateFlagBits.
				VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
			queueFamilyIndex: device.indices.graphicsFamily
		};

		result = device.cmds.CreateCommandPool(
			device.device,
			&commandPoolCreateInfo,
			null,
			&commandPool.pool);
		if (result)
			throw new Exception("Failed to create command pool");

		VkCommandBufferAllocateInfo commmandBufferAllocateInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			commandPool: commandPool.pool,
			commandBufferCount: 1,
			level: VkCommandBufferLevel.VK_COMMAND_BUFFER_LEVEL_PRIMARY
		};

		/* Resize command buffers array to fit command buffer */
		commandPool.buffers.length = 1;

		result = device.cmds.AllocateCommandBuffers(
			device.device,
			&commmandBufferAllocateInfo,
			commandPool.buffers.ptr
		);
		if (result)
			throw new Exception("Failed to allocate command buffers");

		return new DeviceObject!CommandPool(
			device,
			commandPool,
			(Device device, CommandPool commandPool) {
				device.cmds.FreeCommandBuffers(
					device.device,
					commandPool.pool,
					cast(uint)commandPool.buffers.length,
					commandPool.buffers.ptr
				);

				device.cmds.DestroyCommandPool(
					device.device,
					commandPool.pool,
					null
				);
			}
		);
	}

	private void recordCommandBuffer(
		Device device,
		VkCommandBuffer commandBuffer,
		void delegate(VkCommandBuffer commandBuffer) cmds
	) {
		VkResult result;

		VkCommandBufferBeginInfo cmdbufferBeginInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		};

		result = device.cmds.BeginCommandBuffer(commandBuffer, &cmdbufferBeginInfo );
		if (result)
			throw new Exception("Failed to begin command buffer");

		scope(exit) {
			result = device.cmds.EndCommandBuffer(commandBuffer);
			if (result)
				throw new Exception("Failed to end command buffer");
		}

		cmds(commandBuffer);
	}

	private void recordRenderPass(
		Device device,
		VkCommandBuffer commandBuffer,
		void delegate(VkCommandBuffer commandBuffer) renderCmds
	) {
		VkClearValue[] colors = [
			{
				color: {float32: [0.0, 0.0, 0.0, 1.0]}
			}
		];

		VkRenderPassBeginInfo renderPassBeginInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			renderPass: this.renderPass.object,
			framebuffer: this.swapchainFramebuffers.object[this.imageIndex],
			renderArea: {{0, 0}, {this.size[0], this.size[1]}},
			clearValueCount: cast(uint) colors.length,
			pClearValues: colors.ptr
		};

		device.cmds.CmdBeginRenderPass(
			commandBuffer,
			&renderPassBeginInfo,
			VkSubpassContents.VK_SUBPASS_CONTENTS_INLINE
		);
		scope(exit) this.device.cmds.CmdEndRenderPass(commandBuffer);

		renderCmds(commandBuffer);
	}

	private void createDisplayObjects()
	{
		/* Swapchain */
		this.swapchain = this.createSwapchain(
			this.instance,
			this.device,
			this.physicalDevice,
			this.swapchainSupportDetails,
			this.vk_surface.object,
			this.swapchainSurfaceFormat,
			this.size
		);

		uint swapchainImageCount;
		this.device.cmds.GetSwapchainImagesKHR(
			this.device.device,
			this.swapchain.object,
			&swapchainImageCount,
			null
		);
		this.swapchainImages.length = swapchainImageCount;
		this.device.cmds.GetSwapchainImagesKHR(
			this.device.device,
			this.swapchain.object,
			&swapchainImageCount,
			this.swapchainImages.ptr
		);

		this.swapchainImageViews = this.createImageViews(
			device,
			this.swapchainImages,
			this.swapchainSurfaceFormat.format
		);

		/* MSAA */
		this.msaaImage = this.createImage(
			this.device,
			this.instance,
			this.physicalDevice,
			this.size,
			this.swapchainSurfaceFormat.format,
			VkImageType.VK_IMAGE_TYPE_2D,
			this.msaaSamples,
			VkImageTiling.VK_IMAGE_TILING_OPTIMAL,
			VkImageUsageFlagBits.VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT |
			VkImageUsageFlagBits.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			VkMemoryPropertyFlagBits.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		);

		this.pipeline = this.createPipeline(
			this.device,
			this.renderPass.object,
			this.descriptorSetLayout.object,
			this.vert_shader_code,
			this.frag_shader_code,
			this.msaaSamples,
			this.size
		);

		/* Framebuffers */
		this.swapchainFramebuffers = this.createFramebuffers(
			this.device,
			this.renderPass.object,
			this.msaaImage.object.view,
			this.swapchainImageViews.object,
			this.size
		);
	}

	@property uint[2] size()
	{
		return [this.baseSize[0] * this.scale, this.baseSize[1] * scale];
	}

	@property void size(uint[2] size)
	{
		this.baseSize = size;
		this.sizeChanged = true;
	}

	this(WlSurface wl_surface, WlDisplay wl_display, uint[2] size)
	{
		VkImageFormatProperties imageFormatProperties;
		VkResult result;

		super(wl_surface);

		this.size = size;

		/* Load shaders */
		this.vert_shader_code = cast(ubyte[]) read("graphics/renderer/shader/vert.spv");
		this.frag_shader_code = cast(ubyte[]) read("graphics/renderer/shader/frag.spv");

		/* Load global Vulkan commands */
		this.global = loadVulkanGlobalCmds();

		/* Instance */
		this.instance = createInstance(
			this.global,
			this.requiredInstanceExtensionNames,
			this.validationLayerNames);

		this.debugReportCallback = createDebugReportCallback(
			this.instance,
			&debugReportRecieved);

		/* Surface */
		this.vk_surface = createSurface(this.instance, wl_display, this.surface);

		/* Physical device*/
		this.physicalDevice = getPhysicalDevice(
			this.instance,
			wl_display,
			this.requiredDeviceExtensionNames,
			this.vk_surface.object,
			&this.swapchainSupportDetails
		);
		instance.cmds.GetPhysicalDeviceProperties(
			physicalDevice,
			&this.physicalDeviceProperties
		);
		this.queueFamilyIndices = findQueueFamilyIndices(
			this.instance,
			this.physicalDevice,
			wl_display
		);

		/* Device */
		this.device = createDevice(
			this.instance,
			this.physicalDevice,
			this.queueFamilyIndices,
			this.requiredDeviceExtensionNames,
			this.validationLayerNames
		);

		/* Queues */
		device.cmds.GetDeviceQueue(
			device.device,
			this.queueFamilyIndices.graphicsFamily,
			0,
			&this.graphicsQueue);
		device.cmds.GetDeviceQueue(
			device.device,
			this.queueFamilyIndices.presentFamily,
			0,
			&this.presentQueue
		);

		/* Swapchain surface format */
		/* Pick first available surface format as a default */
		this.swapchainSurfaceFormat = this.swapchainSupportDetails.formats[0];

		/* Pick surface format */
		foreach (VkSurfaceFormatKHR surfaceFormat; this.swapchainSupportDetails.formats)
			if (
				surfaceFormat.format == VkFormat.VK_FORMAT_B8G8R8A8_SRGB &&
				surfaceFormat.colorSpace ==
					VkColorSpaceKHR.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
			)
				this.swapchainSurfaceFormat = surfaceFormat;

		/* Get MSAA sample count */
		result = instance.cmds.GetPhysicalDeviceImageFormatProperties(
			this.physicalDevice,
			this.swapchainSurfaceFormat.format,
			VkImageType.VK_IMAGE_TYPE_2D,
			VkImageTiling.VK_IMAGE_TILING_OPTIMAL,
			VkImageUsageFlagBits.VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT |
			VkImageUsageFlagBits.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			0,
			&imageFormatProperties
		);
		if (result)
			throw new Exception("Failed to get image format properties");

		this.msaaSamples = cast(VkSampleCountFlagBits)highestBit(
			imageFormatProperties.sampleCounts
		);

		/* Render Pass */
		this.renderPass = this.createRenderPass(
			this.device,
			this.swapchainSurfaceFormat.format,
			this.msaaSamples
		);

		/* Vertex buffer */
		this.vertexBuffer = this.createBuffer(
			this.device,
			this.instance,
			this.physicalDevice,
			/* TODO: Remove hardcoded size */
			pow(2, 18),
			VkBufferUsageFlagBits.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT
		);

		/* Index buffer */
		this.indexBuffer = this.createBuffer(
			this.device,
			this.instance,
			this.physicalDevice,
			/* TODO: Remove hardcoded size */
			pow(2, 18),
			VkBufferUsageFlagBits.VK_BUFFER_USAGE_INDEX_BUFFER_BIT
		);

		/* Uniform buffer */
		this.uniformBuffer = this.createBuffer(
			this.device,
			this.instance,
			this.physicalDevice,
			/* TODO: Remove hardcoded size */
			pow(2, 12),
			VkBufferUsageFlagBits.VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT
		);

		/* Descriptor sets */
		this.descriptorPool = this.createDescriptorPool(this.device);
		this.descriptorSetLayout = this.createDescriptorSetLayout(this.device);
		this.descriptorSets = this.allocateDescriptorSets(
			this.device,
			this.descriptorPool.object,
			[this.descriptorSetLayout.object]
		);

		VkDescriptorBufferInfo descriptorBufferInfo = {
			buffer: this.uniformBuffer.object.buffer,
			offset: 0,
			range: UBO.sizeof
		};

		VkWriteDescriptorSet writeDescriptorSet = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			dstSet: this.descriptorSets[0],
			dstBinding: 0,
			dstArrayElement: 0,
			descriptorCount: 1,
			descriptorType: VkDescriptorType.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
			pBufferInfo: &descriptorBufferInfo
		};

		device.cmds.UpdateDescriptorSets(device.device, 1, &writeDescriptorSet, 0, null);

		createDisplayObjects();

		/* Command pool */
		this.commandPool = this.createCommandPool(this.device);

		/* Semaphores */
		this.imageAvailableSemaphore = this.createSemaphore(device);
		this.renderFinishedSemaphore = this.createSemaphore(device);
		this.inFlightFence = this.createFence(device);
	}

	@safe pure nothrow void constructElements()
	{
		/* Invalidate old objects */
		this.vertices.destroy();
		this.indices.destroy();
		this.ubos.destroy();

		/* Construct UBOs, vertices and indices */
		foreach (Element element; this.elements) {
			uint vertexCount = cast(uint)vertices.length;

			foreach(vec2 elementVertex; element.vertices) {
				Vertex vertex = {elementVertex, element.color};
				vertices ~= vertex;
			}

			foreach(uint index; element.indices)
				indices ~= index + vertexCount;

			UBO ubo = {
				pos: element.pos * this.scale,
				rot: element.rot,
				scale: element.scale * this.scale
			};

			ubos ~= ubo;
		}
	}

	override void addElement(Element element)
	{
		super.addElement(element);

		this.constructElements();
	}

	override void removeElement(Element element)
	{
		super.removeElement(element);

		this.constructElements();
	}

	override void frame(uint delta)
	{
		super.frame(delta);

		void* data;
		ulong uboOffset;
		VkResult result;

		if (this.sizeChanged) {
			/* Recreate display objects */
			this.device.cmds.DeviceWaitIdle(this.device.device);
			createDisplayObjects();

			this.sizeChanged = false;
		}

		/* TODO: Optimize updating elements */
		this.constructElements();

		result = this.device.cmds.AcquireNextImageKHR(
			this.device.device,
			this.swapchain.object,
			ulong.max,
			this.imageAvailableSemaphore.object,
			null,
			&imageIndex);
		if (result)
			throw new Exception("Failed to acquire next image");

		this.device.cmds.ResetCommandBuffer(this.commandPool.object.buffers[0], 0);

		/* Copy vertices to vertex buffer */
		device.cmds.MapMemory(
			device.device,
			vertexBuffer.object.memory,
			0,
			vertexBuffer.object.size,
			0,
			&data
		);
		memcpy(data, vertices.ptr, vertices.length * vertices[0].sizeof);
		device.cmds.UnmapMemory(device.device, vertexBuffer.object.memory);

		/* Copy indices to index buffer */
		device.cmds.MapMemory(
			device.device,
			indexBuffer.object.memory,
			0,
			indexBuffer.object.size,
			0,
			&data
		);
		memcpy(data, indices.ptr, indices.length * indices[0].sizeof);
		device.cmds.UnmapMemory(device.device, indexBuffer.object.memory);

		/* Copy UBOs to uniform buffer */
		uboOffset = max(
			ubos[0].sizeof,
			physicalDeviceProperties.limits.minUniformBufferOffsetAlignment
		);

		device.cmds.MapMemory(
			device.device,
			uniformBuffer.object.memory,
			0,
			uniformBuffer.object.size,
			0,
			&data
		);
		if (uboOffset != ubos[0].sizeof) {
			ulong offset = 0;
			/* Copy individual UBOs to aligned offsets in device memory */
			foreach(UBO ubo; ubos) {
				memcpy(data + offset, &ubo, ubo.sizeof);
				offset += uboOffset;
			}
		} else
			/* Copy the UBO array directly */
			memcpy(data, ubos.ptr, ubos.length * ubos[0].sizeof);
		device.cmds.UnmapMemory(device.device, uniformBuffer.object.memory);

		this.recordCommandBuffer(
			this.device,
			this.commandPool.object.buffers[0],
			(VkCommandBuffer commandBuffer)
		{
			this.recordRenderPass(
				this.device,
				this.commandPool.object.buffers[0],
				(VkCommandBuffer commandBuffer)
			{
				this.device.cmds.CmdBindPipeline(
					commandBuffer,
					VkPipelineBindPoint.VK_PIPELINE_BIND_POINT_GRAPHICS,
					this.pipeline.object.pipeline
				);

				/* Bind buffers */
				VkBuffer[] vertexBuffers = [this.vertexBuffer.object.buffer];
				VkDeviceSize[] vertexBufferOffsets = [0];
				this.device.cmds.CmdBindVertexBuffers(
					commandBuffer,
					0,
					1,
					vertexBuffers.ptr,
					vertexBufferOffsets.ptr
				);

				this.device.cmds.CmdBindIndexBuffer(
					commandBuffer,
					this.indexBuffer.object.buffer,
					0,
					VkIndexType.VK_INDEX_TYPE_UINT32
				);

				/* Add push constants */
				vec2 windowSize = vec2(this.size[0], this.size[1]);
				this.device.cmds.CmdPushConstants(
					commandBuffer,
					this.pipeline.object.layout,
					VkShaderStageFlagBits.VK_SHADER_STAGE_VERTEX_BIT,
					0,
					windowSize.sizeof,
					&windowSize
				);

				uint indexCount = 0;
				for (int index = 0; index < elements.length; ++index) {
					const uint[1] offsets = [index * cast(uint)uboOffset];

					this.device.cmds.CmdBindDescriptorSets(
						commandBuffer,
						VkPipelineBindPoint.VK_PIPELINE_BIND_POINT_GRAPHICS,
						this.pipeline.object.layout,
						0,
						1,
						this.descriptorSets.ptr,
						1,
						offsets.ptr
					);

					this.device.cmds.CmdDrawIndexed(
						commandBuffer,
						cast(uint) elements[index].indices.length,
						1,
						indexCount,
						0,
						0
					);

					indexCount += cast(uint) elements[index].indices.length;
				}
			});
		});

		VkSemaphore[] graphicsQueueWaitSemaphores = [this.imageAvailableSemaphore.object];
		VkPipelineStageFlags graphicsQueueWaitStages =
			VkPipelineStageFlagBits.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		VkSemaphore[] graphicsQueueSignalSemaphores = [this.renderFinishedSemaphore.object];
		VkSubmitInfo graphicsQueueSubmitInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_SUBMIT_INFO,
			waitSemaphoreCount: cast(uint) graphicsQueueWaitSemaphores.length,
			pWaitSemaphores: graphicsQueueWaitSemaphores.ptr,
			pWaitDstStageMask: &graphicsQueueWaitStages,
			commandBufferCount: 1,
			pCommandBuffers: this.commandPool.object.buffers.ptr,
			signalSemaphoreCount: cast(uint) graphicsQueueSignalSemaphores.length,
			pSignalSemaphores: graphicsQueueSignalSemaphores.ptr
		};

		result = this.device.cmds.QueueSubmit(
			this.graphicsQueue,
			1,
			&graphicsQueueSubmitInfo,
			this.inFlightFence.object
		);
		if (result)
			throw new Exception("Failed to submit command buffer to graphics queue");

		VkPresentInfoKHR presentInfo = {
			sType: VkStructureType.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			waitSemaphoreCount: cast(uint) graphicsQueueSignalSemaphores.length,
			pWaitSemaphores: graphicsQueueSignalSemaphores.ptr,
			swapchainCount: 1,
			pSwapchains: &this.swapchain.object,
			pImageIndices: &this.imageIndex,
		};

		result = this.device.cmds.QueuePresentKHR(this.presentQueue, &presentInfo);
		if (result)
			throw new Exception("Failed to present to swapchain");

		this.device.cmds.WaitForFences(
			device.device,
			1,
			&this.inFlightFence.object,
			true,
			ulong.max
		);
		this.device.cmds.ResetFences(this.device.device, 1, &inFlightFence.object);
	}

	override void onScale(int factor)
	{
		this.scale = factor;
		this.sizeChanged = true;
	}
}

extern(C) nothrow VkBool32 debugReportRecieved(
		VkDebugReportFlagsEXT,
		VkDebugReportObjectTypeEXT,
		ulong,	/* Object */
		size_t,	/* Location */
		int,	/* Message code */
		const char* pLayerPrefix,
		const char* pMessage,
		void* pUserData
) {
	VulkanRenderer renderer = cast(VulkanRenderer) pUserData;

	try {
		writeln(fromStringz(pMessage));
	}
	catch(Exception e) {
		collectException(writeln(e.msg));
	}

	return 0;
}
