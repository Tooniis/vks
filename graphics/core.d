module graphics.core;

import wayland.client;
import std.algorithm;
import std.stdio;
import std.format;

import gl3n.linalg;

class Element
{
	vec2 pos;
	float rot;
	vec2 scale;

	vec3 color;

	immutable vec2[] vertices;
	immutable uint[] indices;

	this(
		vec2 pos,
		vec3 color,
		vec2 scale = vec2(1.0, 1.0),
		immutable vec2[] vertices = [],
		immutable uint[] indices = []
	) {
		this.pos = pos;
		this.rot = 0;
		this.scale = scale;

		this.color = color;

		this.vertices = vertices;
		this.indices = indices;
	}

	void frame(uint delta) { }
}

class Renderer
{
	WlSurface surface;

	Element[] elements;

	private {
		WlCallback onFrame;
		uint lastFrame;
	}

	this(WlSurface wlSurface)
	{
		this.surface = wlSurface;
		/* Register frame callback */
		this.onFrame = this.surface.frame();
		this.onFrame.onDone = (WlCallback callback, uint callbackData) {
			this.lastFrame = callbackData;

			/*
			 * Use 0 for delta on the first frame since lastFrame hasn't
			 * been initialized for delta calculation yet
			 */
			this.frame(0);
			foreach(Element element; this.elements)
				element.frame(0);
		};
	}

	void frame(uint delta) {
		this.onFrame.destroy();
		this.onFrame = this.surface.frame();
		this.onFrame.onDone = (WlCallback callback, uint callbackData) {
			uint delta = callbackData - lastFrame;
			this.lastFrame = callbackData;

			this.frame(delta);
			foreach(Element element; this.elements)
				element.frame(delta);
		};
	}

	void onScale(int factor) { }

	nothrow void addElement(Element element)
	{
		/* Avoid duplicating elements */
		try {
			if (canFind(this.elements, element))
				return;
		} catch(Throwable) {
			return;
		}

		this.elements ~= element;
	}

	nothrow void removeElement(Element element)
	{
		ulong index;

		try {
			index = countUntil(this.elements, element);
		} catch(Throwable) {
			return;
		}

		/* Do nothing if element doesn't exist */
		if (index == -1)
			return;

		this.elements = this.elements.remove(index);
	}
}
