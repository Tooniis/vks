module graphics.element.text;

import graphics.core;
import graphics.element.primitive;
import graphics.element.vector;
import gl3n.linalg;
import std.format;
import std.stdio;
import tess2;
import ttfparser;

/* TODO: Move all vector-related functions to separate object for use in SVG rendering */

class CharacterElement: Element
{
	private immutable TTFPFace face;
	private immutable Glyph glyph;
	private BoxElement bbox;
	private Tess tesselator;

	this(vec2 pos, vec3 color, Tess tesselator, immutable TTFPFace face, ushort glyphId)
	{
		this.tesselator = tesselator;

		this.face = face;

		this.glyph = face.outlineGlyph(glyphId);

		debug
			foreach(Segment segment; this.glyph.outline)
				writeln(segment);

		float height = (this.glyph.bbox[1] - this.glyph.bbox[0]).y;

		this.constructVertices();
		vec2[] vertices = this.tesselator.vertices;
		immutable uint[] indices = cast(immutable uint[])this.tesselator.indices;

		for (ulong index = 0; index < vertices.length; ++index) {
			/* TODO: Implement flipping better */
			vertices[index].y =
				height - vertices[index].y;
		}

		this.bbox = new BoxElement(
			pos + (this.glyph.bbox[0]),
			(this.glyph.bbox[1] - this.glyph.bbox[0]),
			this.color
		);
		/* TODO: Implement proper transform inheritance */
		this.bbox.rot = this.rot;
		this.bbox.scale = this.scale;

		super(pos, color, vertices: cast(immutable vec2[])vertices, indices: indices);
	}

	private pure void constructVertices()
	{
		vec2[] vertices;
		vec2 lastPoint;

		immutable int curveSegments = 3;

		foreach (Segment segment; this.glyph.outline) {
			final switch (segment.type) {
			case SegmentType.MOVE:
				/* New contour. Prepare new vertices array */
				vertices = [];
				goto case;
			case SegmentType.LINE:
				/* Add vertex to contour */
				vertices ~= vec2(segment.x, segment.y);
				break;
			case SegmentType.QUAD:
				/* Sample quadratic bezier curve into vertices */
				for (uint index = 1; index <= curveSegments; ++index)
					vertices ~= quadBezier(
						lastPoint,
						vec2(segment.x1, segment.y1),
						vec2(segment.x, segment.y),
						index / cast(float)curveSegments
					);
				break;
			case SegmentType.CURVE:
				/* Sample cubic bezier curve into vertices */
				for (uint index = 1; index <= curveSegments; ++index)
					vertices ~= cubicBezier(
						lastPoint,
						vec2(segment.x1, segment.y1),
						vec2(segment.x2, segment.y2),
						vec2(segment.x, segment.y),
						index / cast(float)curveSegments
					);
				break;
			case SegmentType.CLOSE:
				/* Contour finished. Submit for tesselation */
				this.tesselator.addContour(vertices);
				break;
			}

			/* Keep track of last point for curve sampling */
			lastPoint = vertices[$-1];
		}

		this.tesselator.tesselate();
	}

	@property BoxElement boundingBox()
	{
		return this.bbox;
	}
}
