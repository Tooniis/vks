module graphics.element.primitive;

import graphics.core;

import gl3n.linalg;

class BoxElement: Element
{
	/* TODO: Make box resizable */
	immutable vec2 size;

	this(vec2 pos, vec2 size, vec3 color)
	{
		this.size = size;

		immutable vec2[] vertices = [
			vec2(0, 0),
			vec2(size.x, 0),
			vec2(size.x, size.y),
			vec2(0, size.y)
		];

		immutable uint[] indices = [0, 1, 2, 2, 3, 0];

		super(pos, color, vertices: vertices, indices: indices);
	}
}
