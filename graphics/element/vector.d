module graphics.element.vector;

import graphics.core;

import std.format;
import std.math;

import gl3n.linalg;
import svgtiny;
import tess2;

@safe pure nothrow vec2 quadBezier(
	vec2 start,
	vec2 control,
	vec2 end,
	float position
) {
	return control + pow(1.0 - position, 2.0) * (start - control)
		+ pow(position, 2.0) * (end - control);
}

@safe pure nothrow vec2 cubicBezier(
	vec2 start,
	vec2 control1,
	vec2 control2,
	vec2 end,
	float position
) {
	return (1.0 - position) * quadBezier(start, control1, control2, position)
		+ position * quadBezier(control1, control2, end, position);
}

class VectorElement: Element
{
	private immutable SVGTinyShape shape;
	private Tess tess;

	this(vec2 pos, vec3 color, vec2 scale = vec2(1.0, 1.0), SVGTinyShape shape)
	{
		this.shape = shape;
		this.tess = new Tess();

		this.constructVertices(pos, this.shape);
		immutable vec2[] vertices = this.tess.vertices;
		immutable uint[] indices = cast(immutable(uint[]))this.tess.indices;

		super(pos, color, scale, vertices, indices);
	}

	private pure void constructVertices(vec2 pos, immutable SVGTinyShape shape)
	{
		vec2[] vertices;
		vec2 lastPoint;

		/* TODO: Make this configurable */
		immutable int curveSegments = 16;

		for (ulong index = 0; index < shape.path.length; ++index) {
			SVGTinyCommand command = cast(SVGTinyCommand)shape.path[index];
			switch (command) {
			case SVGTinyCommand.svgtiny_PATH_MOVE:
				/* New contour. Prepare new vertices array */
				vertices = [];
				goto case;
			case SVGTinyCommand.svgtiny_PATH_LINE:
				vertices ~= vec2(
					cast(int)shape.path[++index],
					cast(int)shape.path[++index]
				);
				break;
			case SVGTinyCommand.svgtiny_PATH_BEZIER:
				/* Sample cubic bezier curve into vertices */
				vec2 p1 = vec2(shape.path[++index], shape.path[++index]);
				vec2 p2 = vec2(shape.path[++index], shape.path[++index]);
				vec2 p3 = vec2(shape.path[++index], shape.path[++index]);
				for (
					uint segmentIndex = 1;
					segmentIndex <= curveSegments;
					++segmentIndex
				)
					vertices ~= cubicBezier(
						lastPoint,
						p1,
						p2,
						p3,
						segmentIndex / cast(float)curveSegments
					);
				break;
			case SVGTinyCommand.svgtiny_PATH_CLOSE:
				/* Contour finished. Submit for tesselation */
				this.tess.addContour(vertices);
				break;
			default:
				throw new Exception("Command %d not supported".format(command));
			}

			/* Keep track of last point for curve sampling */
			lastPoint = vertices[$-1];
		}

		this.tess.tesselate();
	}
}
