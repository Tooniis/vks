module input.core;

import wayland.client;
import std.container;
import std.stdio;

import input.pointer;

abstract class Event
{
	immutable uint serial;
	immutable uint time;

	this(uint serial, uint time)
	{
		this.serial = serial;
		this.time = time;
	}
}

class Input
{
	DList!Event queue;

	protected void frame() { }
}

class Seat
{
	private {
		WlSeat seat;
		WlSeat.Capability capabilities;
		string name;

		Input[] inputs;
	}

	private void onCapabilities(WlSeat wlSeat, WlSeat.Capability capabilities)
	{
		WlPointer pointer;

		this.capabilities = capabilities;

		if (this.capabilities & WlSeat.Capability.pointer) {
			pointer = this.seat.getPointer();
			this.inputs ~= new Pointer(pointer);
		}
	}

	private void onName(WlSeat wlSeat, string name)
	{
		this.name = name;
	}

	this(WlSeat wlSeat)
	{
		this.seat = wlSeat;
		this.seat.onCapabilities = &this.onCapabilities;
		this.seat.onName = &this.onName;
	}

	@property Pointer pointer()
	{
		foreach (Input input; this.inputs) {
			Pointer pointer = cast(Pointer)input;

			if (pointer)
				return pointer;
		}

		return null;
	}
}
