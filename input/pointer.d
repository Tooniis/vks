module input.pointer;

import gl3n.linalg;
import wayland.client;
import wayland.util;

import input.core;

import std.stdio;

class EnterEvent: Event {
	immutable WlSurface surface;
	immutable vec2 pos;

	this(uint serial, WlSurface surface, WlFixed surfaceX, WlFixed surfaceY)
	{
		super(serial, uint.max);

		this.surface = cast(immutable WlSurface)surface;
		this.pos = vec2(cast(double)surfaceX, cast(double)surfaceY);
	}
}

class LeaveEvent: Event {
	immutable WlSurface surface;

	this(uint serial, WlSurface surface)
	{
		super(serial, uint.max);

		this.surface = cast(immutable WlSurface)surface;
	}
}

class MotionEvent: Event {
	immutable vec2 pos;

	this(uint time, WlFixed surfaceX, WlFixed surfaceY)
	{
		super(uint.max, time);

		this.pos = vec2(cast(double)surfaceX, cast(double)surfaceY);
	}
}

class ButtonEvent: Event {
	immutable uint button;
	immutable WlPointer.ButtonState state;

	this(uint serial, uint time, uint button, WlPointer.ButtonState state)
	{
		super(serial, time);

		this.button = button;
		this.state = state;
	}
}

class AxisEvent: Event {
	immutable uint axis;
	immutable uint value;

	this(uint time, WlPointer.Axis axis, WlFixed value)
	{
		super(uint.max, time);

		this.axis = axis;
		this.value = cast(uint)value;

		writeln("axis");
	}
}

class Pointer: Input
{
	private {
		WlPointer pointer;
		vec2 _pos = vec2(0, 0);
	}

	private void onEnter(
		WlPointer wlPointer,
		uint serial,
                WlSurface surface,
                WlFixed surfaceX,
                WlFixed surfaceY
	) {
		this.queue.insert(
			new EnterEvent(serial, surface, surfaceX, surfaceY)
		);
	}

	private void onLeave(
		WlPointer wlPointer,
		uint serial,
                WlSurface surface,
	) {
		this.queue.insert(
			new LeaveEvent(serial, surface)
		);
	}

	private void onMotion(
		WlPointer wlPointer,
		uint time,
		WlFixed surfaceX,
		WlFixed surfaceY
	) {
		this.queue.insert(
			new MotionEvent(time, surfaceX, surfaceY)
		);
	}

	private void onButton(
		WlPointer wlPointer,
		uint serial,
		uint time,
		uint button,
		WlPointer.ButtonState state
	) {
		this.queue.insert(
			new ButtonEvent(serial, time, button, state)
		);
	}

	private void onAxis(
		WlPointer wlPointer,
		uint time,
		WlPointer.Axis axis,
		WlFixed value
	) {
		this.queue.insert(
			new AxisEvent(time, axis, value)
		);
	}

	private void frame(WlPointer pointer)
	{
		while (!this.queue.empty) {
			EnterEvent enterEvent = cast(EnterEvent)this.queue.front;
			LeaveEvent leaveEvent = cast(LeaveEvent)this.queue.front;
			MotionEvent motionEvent = cast(MotionEvent)this.queue.front;
			ButtonEvent buttonEvent = cast(ButtonEvent)this.queue.front;
			AxisEvent AxisEvent = cast(AxisEvent)this.queue.front;

			if  (enterEvent)
				this._pos = enterEvent.pos;
			else if (motionEvent)
				this._pos = motionEvent.pos;

			this.queue.removeFront();
		}

		super.frame();
	}

	@property vec2 pos()
	{
		return this._pos;
	}

	this(WlPointer pointer)
	{
		this.pointer = pointer;
		this.pointer.onEnter = &this.onEnter;
		this.pointer.onLeave = &this.onLeave;
		this.pointer.onMotion = &this.onMotion;
		this.pointer.onButton = &this.onButton;
		this.pointer.onAxis = &this.onAxis;
		this.pointer.onFrame = &this.frame;
	}
}
