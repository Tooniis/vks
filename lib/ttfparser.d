module ttfparser;

import core.stdc.stdint;
import core.stdc.stdlib;
import std.stdio;
import std.string;
import gl3n.linalg;

import include.ttfparser;

struct Glyph {
	Segment[] outline;
	vec2[2] bbox;
};

struct TTFPOutlineBuilder {
	void function(int x, int y, void *data) moveTo;
	void function(int x, int y, void *data) lineTo;
	void function(int x1, int y1, int x, int y, void *data) quadTo;
	void function(int x1, int y1, int x2, int y2, int x, int y, void *data)
		curveTo;
	void function(void *data) closePath;
}

private @trusted nothrow extern(C) {
	void moveTo(float x, float y, void* data)
	{
		Segment[] *segments = cast(Segment[] *)data;
		Segment segment = {
			type: SegmentType.MOVE,
			x: x,
			y: y
		};

		*segments ~= segment;
	}

	void lineTo(float x, float y, void* data)
	{
		Segment[] *segments = cast(Segment[] *)data;
		Segment segment = {
			type: SegmentType.LINE,
			x: x,
			y: y
		};

		*segments ~= segment;
	}

	void quadTo(float x1, float y1, float x, float y, void* data)
	{
		Segment[] *segments = cast(Segment[] *)data;
		Segment segment = {
			type: SegmentType.QUAD,
			x: x,
			y: y,
			x1: x1,
			y1: y1
		};

		*segments ~= segment;
	}

	void curveTo(
		float x1,
		float y1,
		float x2,
		float y2,
		float x,
		float y,
		void* data
	) {
		Segment[] *segments = cast(Segment[] *)data;
		Segment segment = {
			type: SegmentType.CURVE,
			x: x,
			y: y,
			x1: x1,
			y1: y1,
			x2: x2,
			y2: y2
		};

		*segments ~= segment;
	}

	void closePath(void* data)
	{
		Segment[] *segments = cast(Segment[] *)data;
		Segment segment = {
			type: SegmentType.CLOSE,
		};

		*segments ~= segment;
	}
}

enum SegmentType {
	CLOSE = 0,
	MOVE,
	LINE,
	QUAD,
	CURVE
}

struct Segment {
	SegmentType type;
	float x, y;
	float x1, y1;
	float x2, y2;
}

immutable class TTFPFace
{
	private {
		immutable ttfp_face *face;
		ttfp_outline_builder outlineBuilder = {
			move_to: &moveTo,
			line_to: &lineTo,
			quad_to: &quadTo,
			curve_to: &curveTo,
			close_path: &closePath
		};
	}

	this(ubyte[] data, uint index)
	{
		bool ret;

		this.face = cast(immutable ttfp_face *)malloc(ttfp_face_size_of());

		ret = ttfp_face_init(
			cast(const char *)data.ptr,
			data.length,
			index,
			cast(void*)this.face
		);
		if (!ret)
			throw new Exception("Failed to initialize typeface");
	}

	~this()
	{
		free(cast(void *)this.face);
	}

	@property pure nothrow bool regular()
	{
		return ttfp_is_regular(this.face);
	}

	@property pure nothrow bool italic()
	{
		return ttfp_is_italic(this.face);
	}

	@property pure nothrow bool bold()
	{
		return ttfp_is_bold(this.face);
	}

	@property pure nothrow bool oblique()
	{
		return ttfp_is_oblique(this.face);
	}

	@property pure nothrow bool monospaced()
	{
		return ttfp_is_monospaced(this.face);
	}

	pure nothrow ushort glyphIndex(uint codepoint)
	{
		return ttfp_get_glyph_index(this.face, codepoint);
	}

	pure immutable Glyph outlineGlyph(
		ushort glyphId,
	) {
		Glyph glyph;
		ttfp_rect bbox;
		bool ret;

		ret = ttfp_outline_glyph(
			this.face,
			this.outlineBuilder,
			cast(void *) &glyph.outline,
			glyphId,
			&bbox
		);

		if (!ret)
			throw new Exception("Failed to outline glyph");

		glyph.bbox = [
			vec2(bbox.x_min, bbox.y_min),
			vec2(bbox.x_max, bbox.y_max)
		];

		return glyph;
	}
}
