module svgtiny;

import core.stdc.config;
import std.string;

extern (C):

alias svgtiny_colour = int;
enum svgtiny_TRANSPARENT = 0x1000000;

extern (D) auto svgtiny_RGB(T0, T1, T2)(auto ref T0 r, auto ref T1 g, auto ref T2 b)
{
	return r << 16 | g << 8 | b;
}

extern (D) auto svgtiny_RED(T)(auto ref T c)
{
	return (c >> 16) & 0xff;
}

extern (D) auto svgtiny_GREEN(T)(auto ref T c)
{
	return (c >> 8) & 0xff;
}

extern (D) auto svgtiny_BLUE(T)(auto ref T c)
{
	return c & 0xff;
}

private struct svgtiny_shape
{
	float* path;
	uint path_length;
	char* text;
	float text_x;
	float text_y;
	svgtiny_colour fill;
	svgtiny_colour stroke;
	int stroke_width;
}

private struct svgtiny_diagram
{
	int width;
	int height;

	svgtiny_shape *shape;
	uint shape_count;

	ushort error_line;
	const(char) *error_message;
}

enum svgtiny_code
{
	svgtiny_OK = 0,
	svgtiny_OUT_OF_MEMORY = 1,
	svgtiny_LIBDOM_ERROR = 2,
	svgtiny_NOT_SVG = 3,
	svgtiny_SVG_ERROR = 4
}

enum SVGTinyCommand
{
	svgtiny_PATH_MOVE = 0,
	svgtiny_PATH_CLOSE = 1,
	svgtiny_PATH_LINE = 2,
	svgtiny_PATH_BEZIER = 3
}

struct svgtiny_named_color
{
	const(char) *name;
	svgtiny_colour color;
}

extern (C)
{
	svgtiny_diagram* svgtiny_create();
	svgtiny_code svgtiny_parse(
		svgtiny_diagram* diagram,
		const(char)* buffer,
		size_t size,
		const(char)* url,
		int width,
		int height
	);
	void svgtiny_free(svgtiny_diagram* svg);
}

immutable class SVGTinyShape
{
	float[] path;
	string text;
	float[2] textPos;
	int fill;
	int stroke;
	int strokeWidth;

	this(svgtiny_shape shape)
	{
		this.path = cast(immutable float[]) shape.path[0..shape.path_length];
		this.text = cast(string) fromStringz(shape.text);
		this.textPos = [shape.text_x, shape.text_y];
		this.fill = shape.fill;
		this.stroke = shape.stroke;
		this.strokeWidth = shape.stroke_width;
	}
}

class SVGTinyDiagram
{
	private svgtiny_diagram *diagram;

	@property int width() {
		return this.diagram.width;
	}

	@property int height() {
		return this.diagram.height;
	}

	@property SVGTinyShape[] shape() {
		SVGTinyShape[] shape;

		foreach (svgtiny_shape old_shape; this.diagram.shape[0..this.diagram.shape_count]) {
			shape ~= new SVGTinyShape(old_shape);
		}

		return shape;
	}

	this(ubyte[] buffer, string url, int width, int height)
	{
		this.diagram = svgtiny_create();

		svgtiny_code ret = svgtiny_parse(
			this.diagram,
			toStringz(cast(char[]) buffer),
			buffer.length,
			toStringz(url),
			width,
			height
		);

		if (ret)
			throw new Exception("Failed to parse SVG");
	}

	~this()
	{
		svgtiny_free(this.diagram);
	}
}
