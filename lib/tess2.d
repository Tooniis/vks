module tess2;

import gl3n.linalg;

private pure extern (C)
{
	enum TessWindingRule
	{
		TESS_WINDING_ODD,
		TESS_WINDING_NONZERO,
		TESS_WINDING_POSITIVE,
		TESS_WINDING_NEGATIVE,
		TESS_WINDING_ABS_GEQ_TWO,
	};

	enum TessElementType
	{
		TESS_POLYGONS,
		TESS_CONNECTED_POLYGONS,
		TESS_BOUNDARY_CONTOURS,
	};

	enum TessOption
	{
		TESS_CONSTRAINED_DELAUNAY_TRIANGULATION,
		TESS_REVERSE_CONTOURS
	};

	struct TESSalloc;
	struct TESStesselator;

	TESStesselator* tessNewTess(TESSalloc* alloc);
	void tessDeleteTess(TESStesselator *tess);
	void tessAddContour(
		TESStesselator *tess,
		int size,
		const void* pointer,
		int stride,
		int count
	);
	void tessSetOption(TESStesselator *tess, int option, int value);
	int tessTesselate(
		TESStesselator *tess,
		TessWindingRule windingRule,
		TessElementType elementType,
		int polySize,
		int vertexSize,
		const float* normal
	);
	int tessGetVertexCount(TESStesselator *tess);
	float* tessGetVertices(TESStesselator *tess);
	int* tessGetVertexIndices(TESStesselator *tess);
	int tessGetElementCount(TESStesselator *tess);
	int* tessGetElements(TESStesselator *tess);
}

class Tess
{
	private TESStesselator *tess;

	pure this ()
	{
		this.tess = tessNewTess(null);
		tessSetOption(this.tess, TessOption.TESS_CONSTRAINED_DELAUNAY_TRIANGULATION, true);
	}

	pure ~this()
	{
		tessDeleteTess(this.tess);
	}

	pure void addContour(vec2[] vertices)
	{
		tessAddContour(
			this.tess,
			2,
			cast(void *)vertices.ptr,
			cast(int)vertices[0].sizeof,
			cast(int)vertices.length
		);
	}

	pure void tesselate()
	{
		vec3 normal = vec3(0, 0, 1);
		int ret;

		ret = tessTesselate(
			this.tess,
			TessWindingRule.TESS_WINDING_ODD,
			TessElementType.TESS_POLYGONS,
			3,
			2,
			cast(float *)&normal
		);

		if (!ret)
			throw new Exception("Failed to tesselate");
	}

	@property pure vec2[] vertices()
	{
		float *tessVertices;
		vec2[] vertices;

		vertices.length = tessGetVertexCount(this.tess);
		tessVertices = tessGetVertices(this.tess);

		for (ulong index = 0; index < vertices.length; ++index) {
			vertices[index].x = *tessVertices++;
			vertices[index].y = *tessVertices++;
		}

		return vertices;
	}

	@property pure int[] indices()
	{
		int *tessElements;
		int[] elements;

		/*
		 * Tesselator was configured to output 3-vertex elements.
		 * Element indices are triangle indices and can be used to
		 * render the triangles.
		 */
		elements.length = tessGetElementCount(this.tess) * 3;
		tessElements = tessGetElements(this.tess);

		for (ulong index = 0; index < elements.length; ++index)
			elements[index] = *tessElements++;

		return elements;
	}
}
