Experimental barebones Vulkan & Wayland-based graphics framework. Originally made to be part of a Wayland shell.

## Dependencies
- Wayland client protocols
- [wayland-d](https://github.com/rtbo/wayland-d)
- libsvgtiny
- [ttf-parser](https://github.com/RazrFalcon/ttf-parser)
- [gl3n](https://github.com/Dav1dde/gl3n)

## Building
`scons` after installing dependencies in system paths.
