module client;

import std.stdio;
import std.algorithm;
import std.exception;
import core.sys.posix.sys.mman;

import wayland.client;
import wayland.xdg_shell;

private class Surface {
	WlSurface surface;
	private int _scale;

	@property int scale()
	{
		return this._scale;
	}

	@property void scale(int scale)
	{
		if (scale == this._scale)
			return;

		this.surface.setBufferScale(scale);
		this._scale = scale;
	}

	this (WlSurface surface)
	{
		this.surface = surface;
		this._scale = 1;
	}
}

private class Output {
	WlOutput output;
	private int _scale;

	@property int scale()
	{
		return this._scale;
	}

	private void onScale(WlOutput output, int factor)
	{
		this._scale = factor;
	}

	this (WlOutput output)
	{
		this.output = output;
		this.output.onScale = &this.onScale;
	}
}

class Client {
	private {
		WlDisplay display;
		WlCompositor compositor;
		Surface surface;
		Output[] outputs;
		WlOutput[] enteredOutputs;

		WlSeat seat;

		XdgWmBase xdgWmBase;
		XdgSurface xdgSurface;
		XdgToplevel xdgTopLevel;

		immutable void delegate() onConfigure;

		uint[2] size = [640, 480];
	}

	void delegate(int factor) onScale;

	@property WlDisplay wlDisplay()
	{
		return this.display;
	}

	@property WlSurface wlSurface()
	{
		return this.surface.surface;
	}

	@property WlSeat wlSeat()
	{
		return this.seat;
	}

	this(void delegate() onConfigure) {
		/* Load wayland-client */
		wlClientDynLib.load();

		this.onConfigure = onConfigure;

		this.display = WlDisplay.connect();
		WlRegistry registry = display.getRegistry();
		registry.onGlobal = &this.registryGlobal;

		display.roundtrip();

		registry.destroy();

		this.createSurface();
	}

	private void registryGlobal(WlRegistry registry, uint name, string iface, uint ver) {
		if (iface == WlCompositor.iface.name) {
			this.compositor = cast(WlCompositor)registry.bind(
				name, WlCompositor.iface, min(ver, 4)
			);
		} else if (iface == WlOutput.iface.name) {
			WlOutput wlOutput = cast(WlOutput)registry.bind(
				name, WlOutput.iface, min(ver, 2)
			);

			this.outputs ~= new Output(wlOutput);
		} else if (iface == XdgWmBase.iface.name) {
			this.xdgWmBase = cast(XdgWmBase)registry.bind(
				name, XdgWmBase.iface, min(ver, 1)
			);

			this.xdgWmBase.onPing = (XdgWmBase wmbase, uint serial)
			{
				wmbase.pong(serial);
			};
		} else if (iface == WlSeat.iface.name) {
			this.seat = cast(WlSeat)registry.bind(
				name, WlSeat.iface, min(ver, 5)
			);
		}
	}

	private void createSurface() {
		this.surface = new Surface(compositor.createSurface());
		this.surface.surface.onEnter = (WlSurface surface, WlOutput wlOutput)
		{
			this.enteredOutputs ~= wlOutput;

			this.updateSurfaceScale();
		};
		this.surface.surface.onLeave = (WlSurface surface, WlOutput wlOutput)
		{
			this.enteredOutputs = remove(
				this.enteredOutputs,
				countUntil(this.enteredOutputs, wlOutput)
			);

			writeln("onLeave");
			this.updateSurfaceScale();
		};
	}

	private void updateSurfaceScale()
	{
		int maxScale = 1;

		foreach (WlOutput wlOutput; this.enteredOutputs)
			foreach (Output output; this.outputs) {
				if (output.output != wlOutput)
					continue;

				if (output.scale > maxScale)
					maxScale = output.scale;
			}

		this.surface.scale = maxScale;

		WlRegion region = this.compositor.createRegion();
		region.add(0, 0, this.size[0] * maxScale, this.size[1] * maxScale);

		this.surface.surface.setOpaqueRegion(region);

		this.onScale(maxScale);
	}

	void init()
	{

		xdgSurface = xdgWmBase.getXdgSurface(this.surface.surface);
		/* Initial configure handler */
		xdgSurface.onConfigure = (XdgSurface xdgSurface, uint serial)
		{
			xdgSurface.ackConfigure(serial);

			this.onConfigure();

			/*
			 * This handler is only used on the first configure event to commit the
			 * first frame. Remove it to avoid calling it again.
			 */
			/*
			 * TODO: Add permanent configure handler to replace this one after the first
			 * frame.
			 */
			xdgSurface.onConfigure = null;
		};

		xdgTopLevel = xdgSurface.getToplevel();

		/* Commit empty wayland surface to get the XDG surface configured */
		this.surface.surface.commit();
	}

	void run() {
		while (this.display.dispatch() != -1) { }
	}
}
